package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        //TODO: doesn't deal with unary operators -, +

        try {

            ArrayList<Lexeme> infixExpression = StatementToLexemesTranslator.translate(statement);
            PostfixTransformer pt = new PostfixTransformer(infixExpression);
            PostfixCalculator pc = new PostfixCalculator(pt.transform());
            return pc.calculate();

        } catch (Throwable e){
            return null;
        }


    }

}
