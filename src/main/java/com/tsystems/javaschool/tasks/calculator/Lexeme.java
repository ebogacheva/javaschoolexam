package com.tsystems.javaschool.tasks.calculator;

public class Lexeme {

    public enum Type {NUMBER, OPERATOR, OPEN_PAREN, CLOSE_PAREN}

    private String value;
    private Type type;
    private int priority;

    public Lexeme(String value, Type type){

        this.value = value;
        this.type = type;
        if (this.type == Type.OPERATOR && (this.value.equals("+") || this.value.equals("-"))){
            this.priority = 1;
        }
        if (this.type == Type.OPERATOR && (this.value.equals("*") || this.value.equals("/"))){
            this.priority = 2;
        }
    }

    public String getValue() {
        return value;
    }

    public Type getType() {
        return type;
    }

    public int getPriority(){
        return priority;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public double getNumberValue(){
        return Double.parseDouble(value);
    }

    public void setNumberValue(double value){
        setValue(Double.toString(value));
    }

    //TODO: store double value as double not as string
}
