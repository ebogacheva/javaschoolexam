package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class PostfixCalculator {

    private Stack operandsStack;
    private int size;
    private ArrayList<Lexeme> postfix;

    public PostfixCalculator(ArrayList<Lexeme> postfix){

        this.postfix = postfix;
        size = postfix.size();
        operandsStack = new Stack(size);
    }

    public String calculate(){

        for (int i = 0; i < size; i++){
            Lexeme lexeme = postfix.get(i);

            switch (lexeme.getType()){
                case NUMBER:
                    operandsStack.push(lexeme);
                    break;
                case OPERATOR:
                    applyOperator(lexeme.getValue());
                    break;
                default:
                    return null;
            }
        }

        return formatResult(operandsStack.pop().getNumberValue());
    }

    private String formatResult(double value) {
        DecimalFormat format = new DecimalFormat();
        format.setMaximumFractionDigits(4);
        return format.format(value);
    }

    private void applyOperator(String operation){

        double result = 0;
        Lexeme resultLexeme = new Lexeme("", Lexeme.Type.NUMBER);

        double number2 = operandsStack.pop().getNumberValue();
        double number1 = operandsStack.pop().getNumberValue();

        switch (operation){
            case "+":
                result = number1 + number2;
                break;
            case "-":
                result = number1 - number2;
                break;
            case "*":
                result = number1 * number2;
                break;
            case "/":
                if (number2 == 0.0) throw new ArithmeticException();
                else {
                    result = number1 / number2;
                    break;
                }
        }
        resultLexeme.setNumberValue(result);
        operandsStack.push(resultLexeme);
    }



}
