package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;

public class PostfixTransformer {

    private Stack operatorsStack;
    private int size;
    private ArrayList<Lexeme> infix;
    private ArrayList<Lexeme> postfix;

    public PostfixTransformer(ArrayList<Lexeme> infix){

        this.infix = infix;
        size = infix.size();
        operatorsStack = new Stack(size);
        postfix = new ArrayList<>();
    }

    public ArrayList<Lexeme> transform(){

        for (int i = 0; i < size; i++){
            Lexeme currLexeme = infix.get(i);

            switch(currLexeme.getType()){
                case NUMBER:
                    postfix.add(currLexeme);
                    break;
                case OPEN_PAREN:
                    operatorsStack.push(currLexeme);
                    break;
                case CLOSE_PAREN:
                    popOperatorAfterCloseParen();
                    break;
                case OPERATOR:
                    manageOperators(currLexeme);

            }
        }
        while (!operatorsStack.isEmpty()){
            postfix.add(operatorsStack.pop());
        }
        return postfix;
    }

    private void popOperatorAfterCloseParen(){
        while (!operatorsStack.isEmpty()){
            Lexeme lexeme = operatorsStack.pop();
            if (lexeme.getType() == Lexeme.Type.OPEN_PAREN){
                break;
            }
            postfix.add(lexeme);
        }
    }

    private void manageOperators(Lexeme curOperator){
        while (!operatorsStack.isEmpty()){
            Lexeme topOperator = operatorsStack.pop();
            if (topOperator.getValue() == "("){
                operatorsStack.push(topOperator);
                break;
            } else {
                int topPriority = topOperator.getPriority();
                int curPriority = curOperator.getPriority();

                if (topPriority < curPriority) {
                    operatorsStack.push(topOperator);
                    break;
                } else {
                    postfix.add(topOperator);
                }
            }
        }
        operatorsStack.push(curOperator);
    }
}
