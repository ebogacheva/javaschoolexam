package com.tsystems.javaschool.tasks.calculator;

public class Stack {

    private int maxSize;
    private Lexeme[] stackArray;
    private int top;

    public Stack(int size){
        maxSize = size;
        stackArray = new Lexeme[maxSize];
        top = -1;
    }

    public void push(Lexeme obj){
        top++;
        stackArray[top] = obj;
    }

    public Lexeme pop(){
        Lexeme temp = stackArray[top];
        top--;
        return temp;
    }

    public boolean isEmpty(){
        return (top == -1);
    }

}
