package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;

public class StatementToLexemesTranslator {


    private static boolean isOperator(char curChar){

        return (curChar == '*' || curChar == '/' || curChar == '+' || curChar== '-');

    }

    public static ArrayList<Lexeme> translate(String statement){

        final char[] chars = statement.toCharArray();
        ArrayList<Lexeme> lexemes = new ArrayList<>();
        final int size = chars.length;

        for (int i = 0; i < size; i++){

            if (isOperator(chars[i])){
                lexemes.add(new Lexeme(String.valueOf(chars[i]), Lexeme.Type.OPERATOR));
            } else  if (chars[i] == '('){
                lexemes.add(new Lexeme(String.valueOf(chars[i]), Lexeme.Type.OPEN_PAREN));
            } else if (chars[i] == ')'){
                lexemes.add(new Lexeme(String.valueOf(chars[i]), Lexeme.Type.CLOSE_PAREN));
            } else {
                String number = "";
                while (i < size && !(isOperator(chars[i]) || chars[i] == '(' || chars[i] == ')')){
                    number = number + chars[i];
                    i++;
                }
                i--;
                lexemes.add(new Lexeme(number, Lexeme.Type.NUMBER));
            }
        }
        return lexemes;
    }
}

/*
  F  =  A &&  B && ... &&  Z
  !F = !A || !B || ... || !Z

  F  =  A ||  B || ... ||  Z
  !F = !A && !B && ... && !Z

  !(A & B) = !A | !B



 */