package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers == null){
            throw new IllegalArgumentException(); //TODO: not specified behaviour
        }

        if (inputNumbers == null || inputNumbers.isEmpty()) {
            return new int[0][0]; //TODO: not specified behavior. Does empty pyramid matches empty list?
        }

        final int countNumbers = inputNumbers.size();

        final int countRows = calculateRowsCount(countNumbers);

        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);//TODO: new list after sorting

        final int countColumns = countRows + (countRows - 1);

        int[][] result = new int[countRows][countColumns];

        int counter = 0;

        for (int row = 0; row < countRows; row++) {
            int numsInCurRow = row + 1;
            final int zerosInCurRow = countColumns - numsInCurRow;
            int col = (zerosInCurRow - row) / 2;
            while (numsInCurRow > 0) {
                result[row][col] = inputNumbers.get(counter);
                col = col + 2;
                numsInCurRow--;
                counter++;
            }
        }

        return result;
    }

    private static int estimateRowsCount(int numbersCount) {
        return (int) Math.round(Math.sqrt(0.25 + 2 * numbersCount) - 0.5);
    }

    private static int calculateRowsCount (int numbersCount){
        final int estimatedRowsCount = estimateRowsCount(numbersCount);
        final int calculatedNumbersCount = calculateNumbersCount(estimatedRowsCount);
        if (numbersCount == calculatedNumbersCount) {
            return estimatedRowsCount;
        } else {
            throw new CannotBuildPyramidException();
        }
    }

    private static int calculateNumbersCount(int rowsCount){
        return (rowsCount + 1)*rowsCount / 2;
    }
}
